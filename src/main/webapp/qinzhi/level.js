$(function(){

    $('#dg').datagrid({
        url: 'findLevelList.json',
        columns: [[
            {
                field: 'id', title: 'ＩＤ', width: 10, hidden:'true'
            },
            {
                field: 'levelImage', title: '会员级别', width: 20,
                formatter : function(value,row){
                    return '<img src='+value+'   height="75px" width="75px"> <br> '+row.levelName;
                }
            },
            {
                field: 'levelLimit', title: '上传商品数量', width: 60,
            },
            {
                field: 'levelDesc', title: '单次账号充值', width: 50,
            },
            {
                field: 'levelWelfare', title: '充值二维码', width: 40,
                formatter : function(value,row){
                    var level = $("#levelLimit2").val();
                    var other = Number(row.levelLimit);
                    console.log(level)

                    if(null ==level ||""==level){
                        return '<img src='+value+'   height="75px" width="75px">';
                    }
                    var number = Number(level)  ;
                      if( other > number) {
                        return '<img src='+value+'   height="75px" width="75px" > <br>' +
                            ' 请扫描该二维码进行充值 '
                    }else {
                        return  '请选择高级别进行升级 ';
                    };
                }
            },
        ]]
    });
})
function isNumber(value) {
    var patrn = /^(-)?\d+(\.\d+)?$/;
    if (patrn.exec(value) == null || value == "") {
        return false
    } else {
        return true
    }
}
var url;
function updateLevel() {
    $('#dlg').dialog({
        title: '升级',
        modal: true
    });
    $('#dlg').dialog('open').dialog('center');
    $('#fm').form('clear');
}
Date.prototype.toLocaleString = function() {
    return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate();
};


function editLevel() {
    var rows = $('#dg').datagrid('getSelections');
    if (rows && rows.length == 1) {
        $('#fm2').form('clear');
        $("#operatorId").val(rows[0].id);
        $.get("get_level_info.json?id=" + rows[0].id, function (data) {
            $('#dlg2').dialog('open').dialog('center').dialog('setTitle', '编辑会员');
            $('#fm2').form('load', data);
            $('#levelImageUrl').attr( "src",data.levelImage);
            $('#levelWelfareUrl').attr( "src",data.levelWelfare);
        });
        url = 'update.json';
    } else {
        YST_APP.showEditWarnFunc();
    }
}

function deleteLevel() {
    var rows = $('#dg').datagrid('getSelections');
    if (rows && rows.length > 0) {
        $.messager.confirm('确认', '确认要删除吗？', function (r) {
            if (r) {
                var ids = YST_APP.convertIds2Str(rows,"id");
                $.post('delete_level.json', {ids: ids}, function (result) {
                    if (result == "success") {
                        $.messager.show({
                            title: '成功',
                            msg: "删除成功"
                        });
                        $('#dg').datagrid('reload');    // reload the Goods data
                    } else {
                        $.messager.show({
                            title: '失败',
                            msg: "删除失败"
                        });
                    }
                });
            }
        });
    } else {
        YST_APP.showDeleteWarnFunc();
    }
}

function  addLevel() {
    url = 'update.json';
    $('#fm2').form('clear');
    $("#levelWelfareUrl").attr("src","");32
    $("#levelImageUrl").attr("src","");
    $('#dlg2').dialog('open').dialog('center').dialog('setTitle', '新增会员权限');
}

function ajaxFileUpload()
{
    $.ajaxFileUpload({
        url:'uploadImage',//用于文件上传的服务器端请求地址
        secureuri:false ,//一般设置为false
        fileElementId:'goodsImages',//文件上传控件的id属性  <input type="file" id="upload" name="upload" />
        dataType: 'text',//返回值类型 一般设置为json
        success: function (message)  //服务器成功响应处理函数
        {
            layer.msg("上传成功" + message)

            $("#levelImage").val(message);
            $("#levelImageUrl").attr('src',message);
        },
        error: function (data, status, e)//服务器响应失败处理函数
        {
            alert(e);
        }
    });
    return false;
}

function ajaxFileUpload1()
{
    $.ajaxFileUpload({
        url:'uploadImage1',//用于文件上传的服务器端请求地址
        secureuri:false ,//一般设置为false
        fileElementId:'levelWelfares',//文件上传控件的id属性  <input type="file" id="upload" name="upload" />
        dataType: 'text',//返回值类型 一般设置为json
        success: function (message)  //服务器成功响应处理函数
        {
            layer.msg("上传成功" + message)

            $("#levelWelfare").val(message);
            $("#levelWelfareUrl").attr('src',message);
        },
        error: function (data, status, e)//服务器响应失败处理函数
        {
            alert(e);
        }
    });
    return false;
}

function saveGoods() {
    $('#fm2').form('submit', {
        url: "update.json",
        onSubmit: function () {
            var result = $(this).form('validate');
            if(result) {
                $('#dlg2').dialog('close');
            }
            return result;
        },
        success: function (result) {
            if (result == 'success') {
                $.messager.show({
                    title: '成功',
                    msg: "修改会员成功"
                });
            } else {
                $.messager.show({
                    title: '失败',
                    msg: "修改会员失败"
                });
            }
            $('#dg').datagrid('reload');    // reload the Goods data
        }
    });
}