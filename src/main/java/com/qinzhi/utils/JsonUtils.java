package com.qinzhi.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JsonUtils {

	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	public static <T> T parseJson(String in, Class<T> c) {
		return gson.fromJson(in, c);
	}
	
	public static String toJson(Object o) {
		return gson.toJson(o);
	}
}
